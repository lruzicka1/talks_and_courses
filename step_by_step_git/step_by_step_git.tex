\documentclass[15pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[english]{babel}
\usepackage{graphicx}
\usetheme{Boadilla}

\makeatletter
\def\verbatim{\small\@verbatim \frenchspacing\@vobeyspaces \@xverbatim}
\makeatother


\begin{document}
	\author{Lukáš Růžička}
	\title{Git things done now!}
	%\subtitle{}
	%\logo{}
%	\institute{Red Hat}
	\date{summer 2024}
	%\subject{}
	%\setbeamercovered{transparent}
	%\setbeamertemplate{navigation symbols}{}
	\begin{frame}[plain]
		\maketitle
	\end{frame}

\begin{frame}{What is GIT?}
	\begin{itemize}
		\item version control system
		\item open source
		\item actively developed
		\item distributed (decentralized)
		\item created by Linus Torvalds
	\end{itemize}
\end{frame}

\begin{frame}{Why do we want Git?}
    Creating things of a mental nature often involves:
    \vspace{5pt}

    \begin{itemize}
        \item rewriting
        \item deleting
        \item trying blind alleys
        \item reverting to previous versions
        \item revisions and corrections from collaborators
    \end{itemize}

    \vspace{5pt}

    Git can keep all your thoughts and versions together!
\end{frame}
		
	\begin{frame}{Installing Git}
        \texttt{git -{}-version}

        \vspace{5pt}

        to see if Git is already installed, otherwise:

        \vspace{5pt}

		\begin{itemize}
		\item Fedora: sudo dnf install git
		\item Debian: sudo apt-get install git
		\item Mac: http://git-scm.com/download/mac
		\item Windows: http://git-scm.com/download/win
		\end{itemize}
	\end{frame}

	\begin{frame}{Initiating a Git repository}
        Turning the directory into a git repository.

		\begin{itemize}
            \item \texttt{mkdir <repository>}
    		\item \texttt{cd <repository>}
    		\item \texttt{git init}
		\end{itemize}
	\end{frame}

	\begin{frame}{Configuring GIT}
        In order to ease up work, let's configure GIT to suit our needs.

		\begin{itemize}
            \item \texttt{git config -{}-global user.name "Severus Snape"}
            \item \texttt{git config -{}-global user.email "ssnape@hogwarts.edu"}
            \item \texttt{git config -{}-global core.editor vim}
		\end{itemize}
	\end{frame}

	\begin{frame}{Seeing the current status of the repository}
		\begin{itemize}
		\item \texttt{git status}
        \item \texttt{git status -s}
		\end{itemize}
	\end{frame}

	\begin{frame}{Renaming the master branch}
		\begin{itemize}
		\item \texttt{git branch -m main}
		\end{itemize}
	\end{frame}

	\begin{frame}{Creating a first file}
		\begin{itemize}
		\item \texttt{touch README.md}
        \item \texttt{git status}
		\end{itemize}
	\end{frame}

    \begin{frame}{Understanding an \textbf{untracked} file}
        An \textbf{untracked} file in Git is:

        \vspace{5pt}

		\begin{itemize}
		\item seen
        \item not controlled
        \item not touched by Git operations
		\end{itemize}
	\end{frame}
	
	\begin{frame}{Staging the file}
        \textbf{Staging} the file means:
	\begin{itemize}
        \item tell Git that it should \textbf{track} the file
        \item once tracked, Git will \textbf{control} it
        \item \texttt{git add <file>}
        \item \texttt{git add -{}-all}
	\end{itemize}
	\end{frame}

	\begin{frame}{Commiting the file}
        \textbf{Commiting} the file\footnote{show example} means:
	\begin{itemize}
        \item write the staged file into Git history tree
        \item the file will now exist there \textbf{forever}
        \item \texttt{git commit}
        \item alternatively \texttt{git commit -m "Commit message"}
	\end{itemize}
	\end{frame}

	\begin{frame}{Exploring the history}
        All history is recorded in the \textbf{log}. You can see it
        with

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git log}
        \item \texttt{git log -{}-oneline}
	\end{itemize}

    \vspace{5pt}

    Often, we use more sofisticated tools to look into git's history, such as
    \textit{tig, gitk, git-gui,} etc.
	\end{frame}


    \begin{frame}{Ignoring some files}
        You can ignore one or more files using the \textbf{.gitignore} file.
        \begin{itemize}
            \item ignore all .a files \\ \texttt{*.a}
            \item but do track lib.a, even though you're ignoring .a files above \\ \texttt{!lib.a}
            \item only ignore the TODO file in the current directory, not subdir/TODO \\
                \texttt{/TODO}

            \item ignore all files in any directory named build \\
                \texttt{build/}

            \item ignore doc/notes.txt, but not doc/server/arch.txt \\
            \texttt{doc/*.txt}

            \item ignore all .pdf files in the doc/ directory and any of its subdirectories
            \texttt{doc/**/*.pdf}
        \end{itemize}
    \end{frame}


	\begin{frame}{Showing a commit}
        You can explore the content of any commit.

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git show}
        \item \texttt{git show 7c82640}
	\end{itemize}

    \vspace{5pt}
	\end{frame}

	\begin{frame}{Exploring differences}
        You can compare the differences between commits.

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git diff}
        \item \texttt{git diff -{}-staged}
        \item \texttt{git diff b2d1a65 ab13938}\footnote{the direction is always FROM TO.}
	\end{itemize}

    \vspace{5pt}
	\end{frame}

	\begin{frame}{Over to you}
        Now try run the commands we have learnt in your repository.

    \vspace{5pt}

	\begin{itemize}
        \item Create more content.
        \item Stage the new content.
        \item Commit the new content.
        \item Explore the differences between commits.
        \item Explore the commit history.
	\end{itemize}

    \vspace{5pt}
	\end{frame}

	\begin{frame}{Adding content to a previous commit.}
        When you realize that you have commited something and you
        should have commited more\footnote{show example}.

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git commit -{}-amend}
        \item this will break the history
	\end{itemize}

    \vspace{5pt}
	\end{frame}


	\begin{frame}{Undoing stuff}
        When you realize that you have commited something and you
        want to undo the commit, you have two different options.

    \vspace{5pt}

	\begin{itemize}
        \item reverting a commit
        \item resetting a commit
	\end{itemize}

    \vspace{5pt}
	\end{frame}


	\begin{frame}{Reverting a commit}

    \vspace{5pt}

	\begin{itemize}
        \item usually used to revert the last commit, or a row of last commits
        \item reverting earlier commits can result in merge conflicts
        \item produces a new commit that removes the changes made in the reverted commit
        \item does not affect the commit history, so it is safe for shared branches
        \item \texttt{git revert ghj45rt}
	\end{itemize}

    \vspace{5pt}

    See example.
	\end{frame}


	\begin{frame}{Resetting a commit.}

    \vspace{5pt}

	\begin{itemize}
        \item removes staged elements
        \item returns files into the last comitted state
        \item deletes everything upto the mentioned commit
        \item affects the commit history, not suitable for shared branches
	\end{itemize}

    \vspace{5pt}
	\end{frame}

	\begin{frame}{Unstaging a staged file}

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git reset HEAD}
        \item \texttt{git reset HEAD filename.txt}
        \item \texttt{git restore -{}-staged filename.txt}
	\end{itemize}

    \vspace{5pt}
	\end{frame}

	\begin{frame}{Undoing changes on updated file}

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git reset -{}- filename.txt}
        \item \texttt{git reset HEAD}
        \item \texttt{git restore filename.txt}
	\end{itemize}

    \vspace{5pt}
	\end{frame}

	\begin{frame}{Deleting a number of commits going back in time.}

    \vspace{5pt}

	\begin{itemize}
        \item \texttt{git reset abf126f}
        \item \texttt{git reset -{}-soft abf126f}
        \item \texttt{git reset -{}-hard abf126f}
        \item \texttt{git reset -{}-mixed abf126f}
	\end{itemize}

    \vspace{5pt}

    See an example.

    The \texttt{-{}-soft} will keep the changes and only resets the HEAD pointer,
        while the \texttt{-{}-hard} option discards all changes and files. The \texttt{-{}-mixed} option will remove the changes from the commit history, but leaves them in the directory so you can commit them again.

	\end{frame}


	\begin{frame}{Over to you}
        Now try run the commands we have learnt in your repository. Observe
        the status of the repository so that you see the changes.

    \vspace{5pt}

	\begin{itemize}
        \item Commit.
        \item Amend the commit with new content.
        \item Revert the content.
        \item Reset the current state.
        \item Reset to a commit in the past.
	\end{itemize}

    \vspace{5pt}
	\end{frame}




	
	\begin{frame}{Tree and Branches}
    \begin{center}
%	\includegraphics[width=9cm]{tree.png}
	\end{center}
	\end{frame}



	\begin{frame}{What is a branch?}
	\begin{itemize}
		\item alternate development version
		\item it checks out from a certain commit
		\item it can branch from \textbf{master} or another branch
		\item it typically diverges from its origin very quickly
		\item it allows you to work independently
	\end{itemize}
\end{frame}

	\begin{frame}{Explore the branches in your repository}
	\begin{itemize}
		\item \texttt{git branch}
		\item \texttt{git branch -{}-remote}
		\item \texttt{git branch -{}-all}
	\end{itemize}
\end{frame}

\begin{frame}{Create a branch}
	\begin{itemize}
		\item \texttt{git branch development}
	\end{itemize}
\end{frame}

\begin{frame}{Change branches}
	\begin{itemize}
		\item \texttt{git switch bug1234}
		\item \texttt{git checkout bug1234}
		\item \texttt{git checkout -b bug1234}
	\end{itemize}
\end{frame}

	\begin{frame}{Create a branch from a commit in the history}
	\begin{itemize}
		\item \texttt{git checkout df678ok}
		\item \texttt{git branch alternative}
		\item \texttt{git switch alternative}
	\end{itemize}
\end{frame}

\begin{frame}{Merge branch with main}
		
	This technique creates a \textbf{merge commit} in which the \textit{alternative} branch grows back into the \textit{main} branch. It will be visible in the graphical overview.
	
		\vspace{5pt}
	
	\begin{itemize}
		\item \texttt{git checkout main}
		\item \texttt{git merge alternative}
	\end{itemize}
\end{frame}

\begin{frame}{Rebase branch before merge}
	
	This technique will cut the branch off of the tree, it will use all
	the commits from the branch on which we try to rebase, and then it will be placed on top of it.
	
	This creates no merge commit and produces a very clean commit history. The graphical overview will only produce one continuous
	flow.
	
	\vspace{5pt}
	
	\begin{itemize}
		\item \texttt{git checkout alternative}
		\item \texttt{git rebase main}
		\item \texttt{git checkout main}
		\item \texttt{git merge alternative}
	\end{itemize}
\end{frame}

\begin{frame}{Deleting a branch}
	\begin{itemize}
		\item \texttt{git branch -d to-delete}
		\item \texttt{git branch -D to-delete}
	\end{itemize}
\end{frame}

\begin{frame}{Conflicts}
	
	When two branches attempt to change the same file, Git reports a conflict. When branches conflict with each other, they cannot be rebased or merged.
	
	The conflict needs to be fixed before that.
	
	\begin{enumerate}
		\item \texttt{git checkout alternative}
		\item \texttt{git rebase main}
		\item Conflict reported.
		\item Open the conflicting file and fix the conflict.
		\item \texttt{git add conflict\_file}
		\item \texttt{git rebase -{}-continue}
		\item Repeat as needed.
	\end{enumerate}
\end{frame}

\begin{frame}[fragile]{Conflict example}
	\begin{verbatim}
		# List of login names.
		
		### Add your login name to the last available slot.
		
		1.
		2.
		3.
		4.
		5.
	\end{verbatim}
\end{frame}

\begin{frame}[fragile]{Conflict in the file}
	\begin{verbatim}
		# List of login names.
		
		<<<<<<< HEAD
		### Add your login name to the last available slot.
		
		1. pkratoch
		=======
		### Add your login name to the last available slot.
		
		1. lruzicka
		>>>>>>> d8a0beae9626d523d509b9fc53de06c435999d24
		2.
		3.
		4.
		5.
	\end{verbatim}
\end{frame}


\begin{frame}[fragile]{Conflict fixed}
	\begin{verbatim}
		# List of login names.
		
		### Add your login name to the last available slot.
		
		1. pkratoch
		2. lruzicka
		3.
		4.
		5.
	\end{verbatim}
\end{frame}


\begin{frame}{How to minimize conflicts?}
	\begin{itemize}
		\item Work in \textbf{branches}.
		\item Pull in the new changes before you start working.
		\item Rebase your branch on top of main frequently.
	\end{itemize}
	
	\vspace{5pt}
	
	It is \textbf{not} your fault if there is a conflict. These happen all the time.
\end{frame}

\begin{frame}{Squashing commits}
	
	This is helpful, if you do not want to keep the history of your development (in a side branch for instance) and want to squeeze some commits into one bigger commit.
	
	
	\begin{enumerate}
		\item \texttt{git rebase -i a23b34a}
		\item Replace the \textbf{pick} keyword on each line you want to squash with the previous one using the \textbf{squash} or \textbf{fixup} keyword.
		\item Save and close the file.
		\item The commit history is now shorter.
	\end{enumerate}

\end{frame}

\begin{frame}{Changing the commit message}
	
	Sometimes, you need to change the commit message and be more
	precise or anything.
	
	
	\begin{enumerate}
		\item \texttt{git rebase -i a23b34a}
		\item Replace the \textbf{pick} keyword on each line you want to change the commit message for using the \textbf{reword} keyword.
		\item Save and close the file.
		\item Change the commit message.
		\item Save and close the file again.
		\item You have a new commit message.
	\end{enumerate}
	
\end{frame}

\begin{frame}{Working with remote repositories}
	Working with remote repositories requires
	
	\begin{itemize}
		\item an account on a Git server
		\item access rights on a repository
	\end{itemize}
		
\end{frame}

\begin{frame}{Cloning the repository}
		
	\begin{itemize}
		\item \texttt{git clone git@gitlab.com:lruzicka1/repository.git}
		\item access rights on a repository
	\end{itemize}
	
\end{frame}

\begin{frame}[fragile]{Git config}
	\begin{verbatim}
[core]
    repositoryformatversion = 0
    filemode = true
bare = false
    logallrefupdates = true
[remote "origin"]
    url = git@gitlab.com:lruzicka1/talks_and_courses.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[branch "tree"]
    remote = origin
    merge = refs/heads/tree
[branch "git"]
    remote = origin
    merge = refs/heads/git
	\end{verbatim}
\end{frame}

\begin{frame}{Pushing a newly created branch on remote}
	\begin{itemize}
		\item \texttt{git push -{}-set-upstream origin new-branch}
	\end{itemize}	
\end{frame}

\begin{frame}{Deleting a branch on remote}
	\begin{itemize}
		\item \texttt{git push -{}-delete origin branch-to-delete}
	\end{itemize}	
\end{frame}

\begin{frame}{Not being able to push}
	Sometimes, Git will not let you push into the remote because
	\begin{itemize}
		\item your content is different from the remote content, and
		\item your content is older than the remote content, or
		\item your history does not match the remote history
	\end{itemize}	
\end{frame}

\begin{frame}{Synchronising the remote content and local content}
	
	\begin{itemize}
		\item \texttt{git fetch origin}
		\item \texttt{git rebase origin/remote-branch}
		\item \texttt{git push}		
	\end{itemize}	
\end{frame}

\begin{frame}{Overwrite local content with remote content}
	
	\begin{itemize}
		\item \texttt{git fetch origin}
		\item \texttt{git reset -{}-hard origin/remote-branch}
	\end{itemize}	
\end{frame}


\begin{frame}{Overwrite remote content with local content}
	
	\begin{itemize}
		\item \texttt{git push -{}-force}
	\end{itemize}	
\end{frame}


\begin{frame}{Putting content aside before resetting the branch}
	
	\begin{itemize}
		\item \texttt{git reset ghjzdu8} if already commited
		\item \texttt{git stash}
		\item \texttt{git reset -{}-hard origin/remote-branch}
		\item \texttt{git stash apply 0}
	\end{itemize}	
\end{frame}

\begin{frame}{Working with stash}
	
	\begin{itemize}
		\item \texttt{git stash}
		\item \texttt{git stash list}
		\item \texttt{git stash apply 0}
		\item \texttt{git stash drop 0}
	\end{itemize}	
\end{frame}

\begin{frame}{Rescuing deleted content}
	
	\begin{itemize}
		\item \texttt{git reflog}
	\end{itemize}	
\end{frame}


\begin{frame}{Big warning}
	
	Anything you put into Git will stay there forever!!!
	
	Do not push
	\begin{itemize}
		\item passwords
		\item personal info
		\item customer data
		\item and similar
	\end{itemize}
\end{frame}




\begin{frame}{Questions?}
	If you have any questions, just ask now \ldots{}
	
	\vspace{15pt}
	
	\ldots{} or refer to \url{https://git-scm.com/book/en/v2}
\end{frame}

\begin{frame}{}
	Thank you for your attention and have a great day!
\end{frame}





\end{document}
